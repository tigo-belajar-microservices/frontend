package com.artivisi.training.microservice201804.frontend.service;

import com.artivisi.training.microservice201804.frontend.dto.Peserta;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Component
public class PesertaBackendFallback implements PesertaBackendService {

    @Override
    public Iterable<Peserta> dataPeserta() {
        return new ArrayList<>();
    }

    @Override
    public Map<String, String> host() {
        Map<String, String> hostDefault = new HashMap<>();
        hostDefault.put("Host", "localhost");
        hostDefault.put("IP", "127.0.0.1");
        hostDefault.put("Port", "-1");
        return hostDefault;
    }
}
